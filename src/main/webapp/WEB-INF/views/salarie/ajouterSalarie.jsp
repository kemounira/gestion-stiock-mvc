<%@ include file="/WEB-INF/views/include/include.jsp"%>
<!DOCTYPE html>
<html lang="fr">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Ajouter salari�</title>

<!-- Custom fonts for this template-->
<link
	href="<%=request.getContextPath()%>/resources/vendor/fontawesome-free/css/all.min.css"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
	rel="stylesheet">

<!-- Custom styles for this template-->
<link
	href="<%=request.getContextPath()%>/resources/css/sb-admin-2.min.css"
	rel="stylesheet">

</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">
		<!--Appel du menu left-->
		<%@ include file="/WEB-INF/views/menu_left/leftMenu.jsp"%>

		<!-- content wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">
			
			<!-- Main Content -->
			<div id="content">
			
				<!-- Appel au menu top -->
				<%@ include file="/WEB-INF/views/menu_top/topMenu.jsp"%>

				<!-- Begin Page Content -->
				<div class="container-fluid">

					<!-- Page Heading -->
					<h1 class="h3 mb-4 text-gray-800">
						<fmt:message key="common.dashboard" />
					</h1>
				
					<!-- Ajout du Panel -->
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-primary">
								<div class="panel panel-heading">
									<fmt:message key="salaries.nouveau" />
								</div>
								<div class="panel-body">
								</div>
							</div>
						</div>
					</div>
			<div class="row">
          			<div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          			<div class="col-lg-7">
					<div class="p-5">
						<div class="text-center">
							<h1 class="h4 text-gray-900 mb-4">
								<fmt:message key="salarie.ajouter" />
							</h1>
						</div>


						<c:url value="/salarie/enregistrer" var="urlEnregistrer"></c:url>

						<f:form modelAttribute="salarie" action="${urlEnregistrer }"
							method="post">
						<f:hidden path="idsal"/>
						
							<div class="form-group row">
								<div class="col-sm-6 mb-3 mb-sm-0">
									<label><fmt:message key="common.nom" /></label>
									<f:input type="text" path="nomsalarie"
										class="form-control form-control-user"
										placeholder="nomsalarie" />
								</div>
								<div class="col-sm-6">
									<label><fmt:message key="common.prenom" /></label>
									<f:input type="text" path="prenomsalarie"
										class="form-control form-control-user"
										placeholder="prenomsalarie" />
								</div>
							</div>
							<div class="form-group">
								<label><fmt:message key="common.numsecu" /></label>
								<f:input type="number" path="numsecu"
									class="form-control form-control-user"
									placeholder="Numero de s�curit�" />
							</div>				
							
							<div class="panel-footer">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-save">&nbsp;</i>
									<fmt:message key="common.enregistrer" />
								</button>
								&nbsp; &nbsp; <a href="<c:url value="/salarie/"/>"
									class="btn btn-danger"> <i class="fa fa-arrow-left"><i class="fa fa-save">&nbsp;</i><fmt:message
											key="common.annuler" /></i></a>
							</div>						
						</f:form>
						<hr>					
						
					</div>
				</div>
			
		</div>
				
					<!-- /.container-fluid -->

				<!-- End of Main Content -->
		</div>
				<!-- Footer -->
				<footer class="sticky-footer bg-white">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<span>Copyright &copy; Your Website 2019</span>
						</div>
					</div>
				</footer>
				<!-- End of Footer -->
		</div>

			<!-- End of Content Wrapper -->
	</div>	
			
		<!-- End of Page Wrapper -->
</div>	

		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top"> <i
			class="fas fa-angle-up"></i>
		</a>

		<!-- Logout Modal-->
		<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
			aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Ready to
							Leave?</h5>
						<button class="close" type="button" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">Select "Logout" below if you are
						ready to end your current session.</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button"
							data-dismiss="modal">Cancel</button>
						<a class="btn btn-primary" href="login.html">Logout</a>
					</div>
				</div>
			</div>
		</div>

		<!-- Bootstrap core JavaScript-->
		<script
			src="<%=request.getContextPath()%>/resources/vendor/jquery/jquery.min.js"></script>
		<script
			src="<%=request.getContextPath()%>/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

		<!-- Core plugin JavaScript-->
		<script
			src="<%=request.getContextPath()%>/resources/vendor/jquery-easing/jquery.easing.min.js"></script>

		<!-- Custom scripts for all pages-->
		<script
			src="<%=request.getContextPath()%>/resources/js/sb-admin-2.min.js"></script>
</body>

</html>