package com.stock.mvc.controllers;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class RetraiteController {
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	/**
	* Simply selects the home view to render by returning its name.
	*/
	@RequestMapping(value = "/retraite", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {

	return "retraite/retraite";
	}

}
