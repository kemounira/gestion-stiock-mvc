package com.stock.mvc.controllers;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stock.mvc.entity.Salaire;
import com.stock.mvc.services.ISalaireService;



@Controller
@RequestMapping(value = "/salaire", method = RequestMethod.GET)

public class SalaireController {

		@Autowired		
		private ISalaireService salaireservice;
		
		@RequestMapping(value="")
		public String salaire(Model model) {
			List<Salaire> salaires = salaireservice.selectAll();
			if(salaires == null) {
				salaires = new ArrayList<Salaire>();
			}
			model.addAttribute("salaires",salaireservice.selectAll());
			return "salaire/salaire";
		}
		
		@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
		
		public String ajouterSalaire (Model model) {
			Salaire salaire = new Salaire();					
			model.addAttribute("salaire",salaire);
			return "salaire/ajouterSalaire";
		}

		
		@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
		public String enregistrerSalaire (Model model,Salaire salaire) {
			
			if(salaire.getId() != null) {
				salaireservice.update(salaire);
				
			}else {salaireservice.save(salaire);
			
			}	
			return "redirect:/salaire";
		}	
		
		@RequestMapping(value = "/modifier/{id}")
		public String modifierSalaire(Model model, @PathVariable Long id) {
			
			if(id != null) {
				Salaire salaire = salaireservice.getById(id);
				
				if(salaire != null)
				{ model.addAttribute("salaire", salaire);				
				}
			}
			
			return "salaire/ajouterSalaire";
		}
		
		@RequestMapping(value = "/supprimer/{id}")
		public String supprimerSalaire(Model model, @PathVariable Long id) {
			
			salaireservice.remove(id);						
		
			return "redirect:/salaire";
		}
}
