package com.stock.mvc.entity;

import java.io.Serializable;
import java.math.BigDecimal;
//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="table_RETRAITE")

public class Retraite implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	
	@Column(name = "ID")
	private Long id;

	@Column(name = "SECUPLAFOND")
	private BigDecimal secuplafond;

	@Column(name = "SECUDEPLAFOND")
	private BigDecimal secudeplafond;
	
	@Column(name = "COMPLT1")
	private BigDecimal complT1;
	
	@Column(name = "COMPLT2")
	private BigDecimal complT2;

//	@OneToOne(cascade = CascadeType.PERSIST)
//	@JoinColumn(name = "retraite_idSalaire", unique=true)
//	private Salaire salaire;

	public Retraite(BigDecimal secuplafond, BigDecimal secudeplafond, BigDecimal complT1, BigDecimal complT2) {
		super();
		this.secuplafond = secuplafond;
		this.secudeplafond = secudeplafond;
		this.complT1 = complT1;
		this.complT2 = complT2;
//		this.salaire = salaire;
	}

	public Retraite() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getSecuplafond() {
		return secuplafond;
	}

	public void setSecuplafond(BigDecimal secuplafond) {
		this.secuplafond = secuplafond;
	}

	public BigDecimal getSecudeplafond() {
		return secudeplafond;
	}

	public void setSecudeplafond(BigDecimal secudeplafond) {
		this.secudeplafond = secudeplafond;
	}

	public BigDecimal getComplT1() {
		return complT1;
	}

	public void setComplT1(BigDecimal complT1) {
		this.complT1 = complT1;
	}

	public BigDecimal getComplT2() {
		return complT2;
	}

	public void setComplT2(BigDecimal complT2) {
		this.complT2 = complT2;
	}

//	public Salaire getSalaire() {
//		return salaire;
//	}
//
//	public void setSalaire(Salaire salaire) {
//		this.salaire = salaire;
//	}

		

}
