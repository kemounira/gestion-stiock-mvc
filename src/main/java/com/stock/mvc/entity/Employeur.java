package com.stock.mvc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "table_EMPLOYEUR")

public class Employeur implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "ID")
	private Long id;

	@Column(name = "RAISONSOCIALE")
	private String raisonSociale;

	@Column(name = "CODEAPE")
	private String codeAPE;

	@OneToMany(mappedBy = "idemployeur")
	private List<Salaire> salaire;

	public Employeur(Long id, String raisonSociale, String codeAPE) {
		super();
		this.id = id;
		this.raisonSociale = raisonSociale;
		this.codeAPE = codeAPE;
	}

	public Employeur() {
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getCodeAPE() {
		return codeAPE;
	}

	public void setCodeAPE(String codeAPE) {
		this.codeAPE = codeAPE;
	}

	@Override
	public String toString() {
		return "Employeur [id=" + id + ", raisonSociale=" + raisonSociale + ", codeAPE=" + codeAPE + "]";
	}

	

	
}
