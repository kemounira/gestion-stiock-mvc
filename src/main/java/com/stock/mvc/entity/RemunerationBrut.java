package com.stock.mvc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "table_REMUNERATIONBRUTE")

public class RemunerationBrut implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Column(name = "IDRB")
	private Long idrb;
	
	@Column(name = "SALAIREBASE")
	private Long salaireBase;
	
	@Column(name = "ABSENCENOMREM")
	private Long absencenomrem;
	
	@Column(name = "INDEMNITENS")
	private Float indemniteNS;
	
	@Column(name = "PRIME")
	private Float prime;

	public RemunerationBrut(Long salaireBase, Long absencenomrem, Float indemniteNS, Float prime) {
		super();
		this.salaireBase = salaireBase;
		this.absencenomrem = absencenomrem;
		this.indemniteNS = indemniteNS;
		this.prime = prime;
	}

	public RemunerationBrut() {
		super();
	}

	public Long getIdrb() {
		return idrb;
	}

	public void setIdrb(Long idrb) {
		this.idrb = idrb;
	}

	public Long getSalaireBase() {
		return salaireBase;
	}

	public void setSalaireBase(Long salaireBase) {
		this.salaireBase = salaireBase;
	}

	public Long getAbsencenomrem() {
		return absencenomrem;
	}

	public void setAbsencenomrem(Long absencenomrem) {
		this.absencenomrem = absencenomrem;
	}

	public Float getIndemniteNS() {
		return indemniteNS;
	}

	public void setIndemniteNS(Float indemniteNS) {
		this.indemniteNS = indemniteNS;
	}

	public Float getPrime() {
		return prime;
	}

	public void setPrime(Float prime) {
		this.prime = prime;
	}

	@Override
	public String toString() {
		return "RemunerationBrut [idrb=" + idrb + ", salaireBase=" + salaireBase + ", absencenomrem=" + absencenomrem
				+ ", indemniteNS=" + indemniteNS + ", prime=" + prime + "]";
	}


	
}
