package com.stock.mvc.entity;

import java.io.Serializable;
import java.math.BigDecimal;

//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name="table_IMPOT")

public class Impot implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	
	@Column(name = "ID")
	private Long id;

	@Column(name = "NETAVANTIMPOTS")
	private BigDecimal netAvantImpots;

	@Column(name = "TOTALVERSEEMPLOYEUR")
	private BigDecimal totalVerseEmployeur;
	
	@Column(name = "BASEIMPOTS")
	private BigDecimal baseImpots;
	
	@Column(name = "TAUXPERSONALISE")
	private BigDecimal tauxpersonalise;
	
	@Column(name = "MONTANTPRELEVIMPOT")
	private BigDecimal montantPrelevImpot;
	
//	@OneToOne(cascade = CascadeType.PERSIST)
//	@JoinColumn(name = "impots_idSalaire", unique=true)
//	
//	private Salaire salaire;
//	

	public Impot(BigDecimal netAvantImpots, BigDecimal totalVerseEmployeur, BigDecimal baseImpots,
			BigDecimal tauxpersonalise, BigDecimal montantPrelevImpot) {
		super();
		this.netAvantImpots = netAvantImpots;
		this.totalVerseEmployeur = totalVerseEmployeur;
		this.baseImpots = baseImpots;
		this.tauxpersonalise = tauxpersonalise;
		this.montantPrelevImpot = montantPrelevImpot;
//		this.salaire = salaire;
	}

	public Impot() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getNetAvantImpots() {
		return netAvantImpots;
	}

	public void setNetAvantImpots(BigDecimal netAvantImpots) {
		this.netAvantImpots = netAvantImpots;
	}

	public BigDecimal getTotalVerseEmployeur() {
		return totalVerseEmployeur;
	}

	public void setTotalVerseEmployeur(BigDecimal totalVerseEmployeur) {
		this.totalVerseEmployeur = totalVerseEmployeur;
	}

	public BigDecimal getBaseImpots() {
		return baseImpots;
	}

	public void setBaseImpots(BigDecimal baseImpots) {
		this.baseImpots = baseImpots;
	}

	public BigDecimal getTauxpersonalise() {
		return tauxpersonalise;
	}

	public void setTauxpersonalise(BigDecimal tauxpersonalise) {
		this.tauxpersonalise = tauxpersonalise;
	}

	public BigDecimal getMontantPrelevImpot() {
		return montantPrelevImpot;
	}

	public void setMontantPrelevImpot(BigDecimal montantPrelevImpot) {
		this.montantPrelevImpot = montantPrelevImpot;
	}

//	public Salaire getSalaire() {
//		return salaire;
//	}
//
//	public void setSalaire(Salaire salaire) {
//		this.salaire = salaire;
//	}
//
//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}

	
	
	
		
	
}
