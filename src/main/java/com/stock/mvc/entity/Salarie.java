package com.stock.mvc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="table_SALARIE")

public class Salarie implements Serializable {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
		
	@Column(name="IDSAL")
	private Long idsal;
	
	@Column(name="NOMSALARIE")
	private String nomsalarie;
	
	@Column(name="PRENOMSALARIE")
	private String prenomsalarie;
	
	@Column(name="NUMSECU")
	private int numsecu;
	
	@Column(name="EMAIL")
	private String email;	

	@Column(name="TELEPHONE")
	private String telephone;
	
	@ManyToOne()
	@JoinColumn(name = "employeur_id")
	private Employeur employeur;

	public Salarie(Long idsal, String nomsalarie, String prenomsalarie, int numsecu, String email, String telephone,
			Employeur employeur) {
		super();
		this.idsal = idsal;
		this.nomsalarie = nomsalarie;
		this.prenomsalarie = prenomsalarie;
		this.numsecu = numsecu;
		this.email = email;
		this.telephone = telephone;
		this.employeur = employeur;
	}

	public Salarie() {
		super();
	}

	public Long getIdsal() {
		return idsal;
	}

	public void setIdsal(Long idsal) {
		this.idsal = idsal;
	}

	public String getNomsalarie() {
		return nomsalarie;
	}

	public void setNomsalarie(String nomsalarie) {
		this.nomsalarie = nomsalarie;
	}

	public String getPrenomsalarie() {
		return prenomsalarie;
	}

	public void setPrenomsalarie(String prenomsalarie) {
		this.prenomsalarie = prenomsalarie;
	}

	public int getNumsecu() {
		return numsecu;
	}

	public void setNumsecu(int numsecu) {
		this.numsecu = numsecu;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Employeur getEmployeur() {
		return employeur;
	}

	public void setEmployeur(Employeur employeur) {
		this.employeur = employeur;
	}

		
}
	