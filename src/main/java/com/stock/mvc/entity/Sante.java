package com.stock.mvc.entity;

import java.math.BigDecimal;

//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="table_SANTE")
	
public class Sante {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	
	@Column(name = "IDST")
	private Long idst;
	
	@Column(name = "SECUSOCIAL")
	private BigDecimal secuSocial;
	
	@Column(name = "COMPLTA")
	private BigDecimal complTA;
	
	@Column(name = "ID")
	private BigDecimal complTB;
	
	@Column(name = "COMPLSANTE")
	private BigDecimal complSante;
	
	@Column(name = "MALADIEAT")
	private BigDecimal maladieAT;


	public Sante(BigDecimal secuSocial, BigDecimal complTA, BigDecimal complTB, BigDecimal complSante,
			BigDecimal maladieAT) {
		super();
		this.secuSocial = secuSocial;
		this.complTA = complTA;
		this.complTB = complTB;
		this.complSante = complSante;
		this.maladieAT = maladieAT;

	}

	public Sante() {
		super();
	}

	public Long getIdst() {
		return idst;
	}

	public void setIdst(Long idst) {
		this.idst = idst;
	}

	public BigDecimal getSecuSocial() {
		return secuSocial;
	}

	public void setSecuSocial(BigDecimal secuSocial) {
		this.secuSocial = secuSocial;
	}

	public BigDecimal getComplTA() {
		return complTA;
	}

	public void setComplTA(BigDecimal complTA) {
		this.complTA = complTA;
	}

	public BigDecimal getComplTB() {
		return complTB;
	}

	public void setComplTB(BigDecimal complTB) {
		this.complTB = complTB;
	}

	public BigDecimal getComplSante() {
		return complSante;
	}

	public void setComplSante(BigDecimal complSante) {
		this.complSante = complSante;
	}

	public BigDecimal getMaladieAT() {
		return maladieAT;
	}

	public void setMaladieAT(BigDecimal maladieAT) {
		this.maladieAT = maladieAT;
	}

		
	
	

}
