package com.stock.mvc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="table_SALAIRE")

public class Salaire implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Column(name="ID")
	private Long id;
	
	@Column(name="CONGEPAYE")
	private Float congepaye;	
	
	@Column(name="SALAIRENET")
	private Float salaireNet;
	
	@Column(name="NETAPAYER")
	private Float netapayer;
	
	@Column(name="ELEMENTNONSOUMISACOTISATION")
	private String elementNonSoumisACotisation;
	
	@Column(name="DATENETPAYER")
	private String datenetpayer;
	
	@Column(name="IDEMPLOYEUR", nullable = false)
	private Employeur idemployeur;
	
		
//	@OneToOne(mappedBy="id_Salaire")
//	private Retraite Retraite;
//	
//	@OneToOne(mappedBy="id_Salaire")
//	private Sante sante;

	
	public Salaire() {
		super();
	}


	public Salaire(Float congepaye, Float salaireNet, Float netapayer, String elementNonSoumisACotisation,
			String datenetpayer, Employeur idemployeur) {
		super();
		this.congepaye = congepaye;
		this.salaireNet = salaireNet;
		this.netapayer = netapayer;
		this.elementNonSoumisACotisation = elementNonSoumisACotisation;
		this.datenetpayer = datenetpayer;
		this.idemployeur = idemployeur;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Float getCongepaye() {
		return congepaye;
	}


	public void setCongepaye(Float congepaye) {
		this.congepaye = congepaye;
	}


	public Float getSalaireNet() {
		return salaireNet;
	}


	public void setSalaireNet(Float salaireNet) {
		this.salaireNet = salaireNet;
	}


	public Float getNetapayer() {
		return netapayer;
	}


	public void setNetapayer(Float netapayer) {
		this.netapayer = netapayer;
	}


	public String getElementNonSoumisACotisation() {
		return elementNonSoumisACotisation;
	}


	public void setElementNonSoumisACotisation(String elementNonSoumisACotisation) {
		this.elementNonSoumisACotisation = elementNonSoumisACotisation;
	}

	public String getDatenetpayer() {
		return datenetpayer;
	}


	public void setDatenetpayer(String datenetpayer) {
		this.datenetpayer = datenetpayer;
	}


	public Employeur getIdemployeur() {
		return idemployeur;
	}

	public void setIdemployeur(Employeur idemployeur) {
		this.idemployeur = idemployeur;
	}

	
	}

	