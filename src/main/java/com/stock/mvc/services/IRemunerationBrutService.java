package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entity.RemunerationBrut;


public interface IRemunerationBrutService {

	public RemunerationBrut save(RemunerationBrut entity);
	public RemunerationBrut update(RemunerationBrut entity);
	public List<RemunerationBrut> selectAll() ;
	public List<RemunerationBrut> selectAll(String sortField, String sort);
	//recuperer les element en fonction de leur ID
	public RemunerationBrut getById(Long id);
	
	public void remove(Long id);
	//Recuperer les objets (une listeet leur valeurs
	
	public RemunerationBrut findOne (String paramName, Object[] paramValue); //on recherche les nom des salarie(liste de nom) dans une lisete 
	public RemunerationBrut findOne (String[] paramNames, Object[] paramValues);
	public RemunerationBrut findOne (String paramName, String paramValues);
	public int findCountBy(String paramName, String paramValue);
}
