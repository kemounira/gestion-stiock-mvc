package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entity.Salarie;

public interface ISalarieService {

	public Salarie save(Salarie entity);
	public Salarie update(Salarie entity);
	public List<Salarie> selectAll() ;
	public List<Salarie> selectAll(String sortField, String sort);
	//recuperer les element en fonction de leur ID
	public Salarie getById(Long id);
	
	public void remove(Long id);
	//Recuperer les objets (une listeet leur valeurs
	
	public Salarie findOne (String paramName, Object[] paramValue); //on recherche les nom des salarie(liste de nom) dans une lisete 
	public Salarie findOne (String[] paramNames, Object[] paramValues);
	public Salarie findOne (String paramName, String paramValues);
	public int findCountBy(String paramName, String paramValue);

}
