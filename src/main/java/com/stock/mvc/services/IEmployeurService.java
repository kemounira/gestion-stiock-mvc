package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entity.Employeur;



public interface IEmployeurService {
	
	public Employeur save(Employeur entity);
	public Employeur update(Employeur entity);
	public List<Employeur> selectAll() ;
	public List<Employeur> selectAll(String sortField, String sort);
	//recuperer les element en fonction de leur ID
	public Employeur getById(Long id);
	
	public void remove(Long id);
	//Recuperer les objets (une listeet leur valeurs
	
	public Employeur findOne (String paramName, Object[] paramValue); //on recherche les nom des salarie(liste de nom) dans une lisete 
	public Employeur findOne (String[] paramNames, Object[] paramValues);
	public Employeur findOne (String paramName, String paramValues);
	public int findCountBy(String paramName, String paramValue);


}


