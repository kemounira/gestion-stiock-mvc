package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entity.Retraite;


public interface IRetraiteService {

	public Retraite save(Retraite entity);
	public Retraite update(Retraite entity);
	public List<Retraite> selectAll() ;
	public List<Retraite> selectAll(String sortField, String sort);
	//recuperer les element en fonction de leur ID
	public Retraite getById(Long id);
	
	public void remove(Long id);
	//Recuperer les objets (une listeet leur valeurs
	
	public Retraite findOne (String paramName, Object[] paramValue); //on recherche les nom des salarie(liste de nom) dans une lisete 
	public Retraite findOne (String[] paramNames, Object[] paramValues);
	public Retraite findOne (String paramName, String paramValues);
	public int findCountBy(String paramName, String paramValue);

}
