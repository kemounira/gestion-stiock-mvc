package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IRemunerationBrutDao;
import com.stock.mvc.entity.RemunerationBrut;
import com.stock.mvc.services.IRemunerationBrutService;

@Transactional
public class RemunerationBrutServiceImpl implements IRemunerationBrutService {
	
	 // j app mon objet dao
	
	private IRemunerationBrutDao dao;	
	
	

	public IRemunerationBrutDao getDao() {
		return dao;
	}

	public void setDao(IRemunerationBrutDao dao) {
		this.dao = dao;
	}

	@Override
	public RemunerationBrut save(RemunerationBrut entity) {
		return dao.save(entity);
	}

	@Override
	public RemunerationBrut update(RemunerationBrut entity) {
		return dao.update(entity);
	}

	@Override
	public List<RemunerationBrut> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<RemunerationBrut> selectAll(String sortField, String sort) {

		return dao.selectAll(sortField, sort);
	}

	@Override
	public RemunerationBrut getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);		
	}

	@Override
	public RemunerationBrut findOne(String paramName, Object[] paramValue) {
		return null;
	}

	@Override
	public RemunerationBrut findOne(String[] paramNames, Object[] paramValues) {

		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public RemunerationBrut findOne(String paramName, String paramValue) {

		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}


	

}
