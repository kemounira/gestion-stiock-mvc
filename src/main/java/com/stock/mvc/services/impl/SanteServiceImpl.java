package com.stock.mvc.services.impl;

import java.util.List;


import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ISanteDao;
import com.stock.mvc.entity.Sante;

@Transactional
public class SanteServiceImpl implements ISanteDao{

	private ISanteDao dao;

	
	
	public ISanteDao getDao() {
		return dao;
	}

	public void setDao(ISanteDao dao) {
		this.dao = dao;
	}

	@Override
	public Sante save(Sante entity) {
		return dao.save(entity);
	}

	@Override
	public Sante update(Sante entity) {
		return dao.update(entity);
	}

	@Override
	public List<Sante> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Sante> selectAll(String sortField, String sort) {

		return dao.selectAll(sortField, sort);
	}

	@Override
	public Sante getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);		
	}

	@Override
	public Sante findOne(String paramName, Object[] paramValue) {
		return null;
	}

	@Override
	public Sante findOne(String[] paramNames, Object[] paramValues) {

		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public Sante findOne(String paramName, String paramValue) {

		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}


}
