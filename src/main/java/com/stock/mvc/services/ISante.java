package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entity.Sante;


public interface ISante {

	public Sante save(Sante entity);
	public Sante update(Sante entity);
	public List<Sante> selectAll() ;
	public List<Sante> selectAll(String sortField, String sort);
	//recuperer les element en fonction de leur ID
	public Sante getById(Long id);
	
	public void remove(Long id);
	//Recuperer les objets (une listeet leur valeurs
	
	public Sante findOne (String paramName, Object[] paramValue); //on recherche les nom des salarie(liste de nom) dans une lisete 
	public Sante findOne (String[] paramNames, Object[] paramValues);
	public Sante findOne (String paramName, String paramValues);
	public int findCountBy(String paramName, String paramValue);

}
