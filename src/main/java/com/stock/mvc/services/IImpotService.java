package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entity.Impot;


public interface IImpotService {

	public Impot save(Impot entity);
	public Impot update(Impot entity);
	public List<Impot> selectAll() ;
	public List<Impot> selectAll(String sortField, String sort);
	//recuperer les element en fonction de leur ID
	public Impot getById(Long id);
	
	public void remove(Long id);
	//Recuperer les objets (une listeet leur valeurs
	
	public Impot findOne (String paramName, Object[] paramValue); //on recherche les nom des salarie(liste de nom) dans une lisete 
	public Impot findOne (String[] paramNames, Object[] paramValues);
	public Impot findOne (String paramName, String paramValues);
	public int findCountBy(String paramName, String paramValue);

}
