package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entity.Salaire;


public interface ISalaireService {
	
	public Salaire save(Salaire entity);
	public Salaire update(Salaire entity);
	public List<Salaire> selectAll() ;
	public List<Salaire> selectAll(String sortField, String sort);
	//recuperer les element en fonction de leur ID
	public Salaire getById(Long id);
	
	public void remove(Long id);
	//Recuperer les objets (une listeet leur valeurs
	
	public Salaire findOne (String paramName, Object[] paramValue); //on recherche les nom des salarie(liste de nom) dans une lisete 
	public Salaire findOne (String[] paramNames, Object[] paramValues);
	public Salaire findOne (String paramName, String paramValues);
	public int findCountBy(String paramName, String paramValue);

}
