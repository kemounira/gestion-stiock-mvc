package com.stock.mvc.dao.Impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.stock.mvc.dao.IGenericDao;

@SuppressWarnings("unchecked")
public class GenericDaoImpl<T> implements IGenericDao<T> {
	
	@PersistenceContext
	
	EntityManager em;
	private Class<T> type;
	
	
	public GenericDaoImpl() {
		super();
		Type f = getClass().getGenericSuperclass(); //je recupere ma classe et je lui demande de la mettre en type
		ParameterizedType pt = (ParameterizedType) f;//je cr�e mon objet pt en faisant un caste
		type = (Class<T>)pt.getActualTypeArguments() [0];//je recup�re l'argumentactuel de notre type qui se trouve dans le position 0
	}

	//On cr�e le getter de notre type 
	public Class<T> getType() {
		return type;
	}

	@Override
	public T save(T entity) {
		//Enregistre une entit� T
		em.persist(entity);
		return entity;
	}

	@Override
	public T update(T entity) {		
		return em.merge(entity);
	}

	@Override
	public List<T> selectAll() {
		//je r�cup�re mon objet grace � create query, je selectionne tous les element de mon objet salari�
		Query query = em.createQuery("select t from "+type.getSimpleName() +" t");
		return query.getResultList(); //Retourne une liste de type T
	}

	@Override
	public List<T> selectAll(String sortField, String sort) {
		return null;
	}

	@Override
	public T getById(Long id) {		
		return em.find(type, id);
	}

	@Override
	public void remove(Long id) {
		T toto = em.getReference(type, id);
		em.remove(toto);
	}

	@Override
	public T findOne(String paramName, Object[] paramValue) {
//		Query query = em.createQuery("select t from"+type.getSimpleName() +" t");
		return null;
	}

	@Override
	public T findOne(String paramName, String paramValues) {
		Query query = em.createQuery("select t from "+type.getSimpleName() +
				" t where" +paramName+"=:x");
		query.setParameter("x", paramValues);
		return query.getResultList().size()>0 ? (T)query.getResultList().get(0): null;
	}

	@Override
	public T findOne(String[] paramNames, Object[] paramValues) {
		if(paramNames.length != paramValues.length) {
		return null;}
		String queryString = "select t from "+type.getSimpleName()+ " t where";
		int len = paramNames.length;
		for(int i=0;i<len; i++) {
			queryString +="e."+paramNames[i]+"=:x"+i;
			if((i+1)<len) {
				queryString += "and";				
			}
		}
		Query query = em.createQuery(queryString);
		for(int i=0;i<paramValues.length; i++) {
			query.setParameter("x"+i, paramValues[i]);
			}
		return query.getResultList().size()>0 ? (T)query.getResultList().get(0): null;
	}

	@Override
	//Rechercher le nombre de salari� ayant le nom x
	public int findCountBy(String paramName, String paramValue) {
		Query query = em.createQuery("select t from "+type.getSimpleName() +
				" t where" +paramName+"=:x");
		query.setParameter(paramName, paramValue); //retourne le nombre d'�lements contenu dans la liste de sal
		
		return query.getResultList().size()>0 ? ((Long) query.getSingleResult()).intValue(): null;
	
	}
}
