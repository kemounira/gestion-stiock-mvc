package com.stock.mvc.dao;

import com.stock.mvc.entity.Employeur;

public interface IEmployeurDao extends IGenericDao<Employeur> {

}
