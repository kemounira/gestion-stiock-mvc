package com.stock.mvc.dao;

import java.util.List;

public interface IGenericDao<T> {
 
	//Ajout des CRUD
	public T save(T entity);
	public T update(T entity);
	public List<T> selectAll() ;
	public List<T> selectAll(String sortField, String sort);
	//recuperer les element en fonction de leur ID
	public T getById(Long id);
	
	public void remove(Long id);
	//Recuperer les objets (une listeet leur valeurs
	
	public T findOne(String paramName, Object[] paramValue); //on recherche les nom des salarie(liste de nom) dans une lisete 
	public T findOne(String[] paramNames, Object[] paramValues);
	public T findOne(String paramName, String paramValues);
	public int findCountBy(String paramName, String paramValue);
	
}
